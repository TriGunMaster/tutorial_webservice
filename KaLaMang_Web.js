var bodyparser = require('body-parser');
var express = require('express');
var app = express();

var cookieParser = require('cookie-parser');
var session = require('express-session');
var morgan = require('morgan');

app.set('view engine', 'ejs');
app.set('view options', { layout: false });
app.use('/img', express.static(__dirname + '/views/img'));
app.use('/css', express.static(__dirname + '/views/css'));
app.use(bodyparser.urlencoded({ extended: true }));

app.use(cookieParser());
app.use(morgan('dev'));

var mongodb = require('mongodb');
var dbuser = 'kalamangstudio';
var dbpass = '123456';
var dbhost = 'localhost';
var dbport = '27017';
var dbname = 'KaLaMangDB';
var server = 'mongodb://' + dbuser + ':' + dbpass + '@' + dbhost + ':' + dbport + '/' + dbname;

app.listen(80, function () {
    console.log('Server Running... For WebService');
    m_Test();
});

app.use(session({
    key: 'connect',
    secret: 'randomkalamang',
    resave: false,
    saveUninitialized: false,
    cookie: {
        expires: 300000
    }
}));

app.use((req, res, next) => {
    if (req.cookies.connect && !req.session.acc_Email) {
        //res.clearCookie('connect');
        console.log('Cookie Clear');
    }
    next();
});

var sessionChecker = (req, res, next) => {
    if (!req.session.acc_Email && req.cookies.connect) {
        res.redirect('/');
    } else {
        next();
    }
};

app.get('/', function (req, res) {
    var sendData = {};
    if (!req.session.views)
        req.session.views = 0;
    req.session.views++;

    if (req.session.acc_Email && req.session.acc_GradeID && req.session.ejs_Action == 'LoginComplete') {
        // Loging
        sendData = {
            ShowViews: req.session.views, MsgServer: req.session.ejs_Action,
            Email: req.session.acc_Email, GradeID: req.session.acc_GradeID,
            NickName: req.session.acc_NickName,

            Page: req.session.ejs_Page
        };
        if (req.session.ejs_sendData)
            sendData = Object.assign(sendData, req.session.ejs_sendData);

        console.log('Loging: ' + req.session.acc_Email + ' | CookieExp: ' + new Date(req.session.cookie.expires).toISOString());
    }
    else if (req.session.ejs_Action == 'LoginFail') {
        // Login Fail
        switch (req.session.ejs_Action_No) {
            case 1: // Email Wrong
                sendData = { ShowViews: req.session.views, MsgServer: 'Email Wrong' };
                break;
            case 2: // Password Wrong
                sendData = { ShowViews: req.session.views, MsgServer: 'Password Wrong' };
                break;
        }
        console.log('Login Fail: ' + req.session.ejs_Action + '| No: ' + req.session.ejs_Action_No);
    }
    else if (req.session.ejs_Action == 'CallLogout') {
        // Logout
        console.log('Logout: ' + req.session.acc_Email + ' | Date: ' + new Date().toISOString());
        req.session.destroy(function (err) {
            // cannot access session here
        });
        sendData = { ShowViews: '0', MsgServer: 'Logout Complete' };
        res.render('index', { sendData });
        return;
    }
    else if (req.session.acc_Email && req.session.acc_GradeID && req.cookies.connect) {
        // Logged
        sendData = {
            ShowViews: req.session.views, MsgServer: 'Refresh on Alert Login',
            Email: req.session.acc_Email, GradeID: req.session.acc_GradeID,
            NickName: req.session.acc_NickName,

            Page: req.session.ejs_Page
        };
        if (req.session.ejs_sendData)
            sendData = Object.assign(sendData, req.session.ejs_sendData);

        console.log('Logged: ' + req.session.acc_Email + ' | CookieExp: ' + new Date(req.session.cookie.expires).toISOString());
    }
    else {
        // No Login
        sendData = { ShowViews: req.session.views, MsgServer: 'NO Login' };
        console.log('NoLogin' + ' | ' + req.session.acc_Email + ' | ' + req.session.acc_GradeID + ' | ' + req.session.ejs_Action + ' | ' + req.cookies.connect);
    }

    req.session.ejs_Action = '';
    res.render('index', { sendData });
});

app.post('/action_login', function (req, res) {

    mongodb.MongoClient.connect(server, { useNewUrlParser: true }, function (err, client) {
        if (err) { console.log(err); }
        var db = client.db('KaLaMangDB');

        db.collection('KLM_Account').findOne({ UserName: req.body.email }, function (err, doc) {
            if (err) { console.log(err); }

            if (doc) {
                if (req.body.email == doc.UserName) {
                    if (req.body.psw == doc.PassWord) {
                        // Login Success Data Save
                        req.session.acc_Email = doc.UserName;
                        req.session.acc_NickName = doc.NickName
                        req.session.acc_GradeID = doc.Age;

                        // Ejs Status Action
                        req.session.ejs_Action = 'LoginComplete';
                        if (!req.session.views)
                            req.session.views = 1;

                        console.log('Login Success');
                    }
                    else {
                        // Pass wrong
                        req.session.ejs_Action = 'LoginFail';
                        req.session.ejs_Action_No = 2;
                        console.log('Pass Wrong');
                    }
                }
            }
            else {
                // Email Wrong
                req.session.ejs_Action = 'LoginFail';
                req.session.ejs_Action_No = 1;
                console.log('Email Wrong');
            }

            console.log('POST Receiver: Email: ' + req.body.email + " Password: " + req.body.psw + ' TimeView: ' + req.session.views);
            client.close();
            res.redirect('/');
        });
        console.log("Mongo Run Success");
    }); // End MongoDB Connect
}); // End action_pass

app.get('/action_logout', function (req, res) {
    req.session.ejs_Action = 'CallLogout';
    res.redirect('/');
});

app.route('/action_profile').get(sessionChecker, (req, res) => {
    mongodb.MongoClient.connect(server, { useNewUrlParser: true }, function (err, client) {
        if (err) { console.log(err); }
        var db = client.db('KaLaMangDB');

        db.collection('KLM_Account').findOne({ UserName: req.session.acc_Email }, function (err, doc) {
            if (err) { console.log(err); }

            var sendData = {};
            var ChangeProfileShow = false;
            req.session.views++;

            if (req.query.editprofile)
                ChangeProfileShow = true;

            sendData = { ShowViews: req.session.views, MsgServer: 'Show Data My Profile', GetNickName: doc.NickName, GetGradeID: doc.Age, GetActionChangeProfile: ChangeProfileShow };

            console.log(sendData);
            client.close();
            res.render('profile', { sendData });
        });
        console.log("Mongo Run Success");
    }); // End MongoDB Connect
}).post((req, res) => {
    mongodb.MongoClient.connect(server, { useNewUrlParser: true }, function (err, client) {
        if (err) { console.log(err); }
        var db = client.db('KaLaMangDB');

        db.collection('KLM_Account').updateOne(
            { UserName: req.session.acc_Email },
            { $set: { NickName: req.body.nickname, Age: parseInt(req.body.age) } }
            , function (err, docs) {
                var sendData = {};
                req.session.views++;
                if (err) {
                    var i = 0;
                    for (var key in err) {
                        console.log("Data: " + (++i)
                            + " ###MESSAGE_ERROR### Key: " + key);
                        console.log(err[key]);
                    }
                }
                for (var key in docs) {
                    console.log("Data: " + (++i)
                        + " ###MESSAGE### Key: " + key);
                    console.log(docs[key]);
                }
                sendData = { ShowViews: req.session.views, MsgServer: 'Change Profile Complete', GetNickName: req.body.nickname, GetGradeID: req.body.age };
                client.close();
                res.render('profile', { sendData });
            });
        console.log("Mongo Run Success");
    }); // End MongoDB Connect
});

app.get('/action_myitem', function (req, res) {
    mongodb.MongoClient.connect(server, { useNewUrlParser: true }, function (err, client) {
        if (err) { console.log(err); }
        var db = client.db('KaLaMangDB');

        var sendData = {};
        var getdata = '';
        req.session.views++;

        db.collection('KLM_Account').aggregate([
            { $match: { 'UserName': req.session.acc_Email } },
            {
                $lookup: {
                    from: 'KLM_Inventory',
                    localField: 'AID',
                    foreignField: 'OwnAID',
                    as: 'new_invent'
                }
            },
            {
                $lookup: {
                    from: 'KLM_ItemID',
                    localField: 'new_invent.ItemID',
                    foreignField: 'ItemID',
                    as: 'new_itemid'
                }
            },
            {
                $group: {
                    _id: '$new_invent.InvenID',
                    ItemID: { $first: '$new_invent.ItemID' },
                    ItemName: { $first: '$new_itemid' },
                    Stack: { $first: '$new_invent.Stack' },
                }
            }
        ]).toArray(function (err, docs) {
            if (err) { console.log(err); }

            // Create Array ItemName
            var ItemNames = {};

            for (var key in docs) {
                var i = 0;

                var ItemInfoArray = docs[key].ItemName;
                for (var keyItem in ItemInfoArray) {
                    ItemNames[ItemInfoArray[keyItem].ItemID] = ItemInfoArray[keyItem].ItemName;
                }

                for (var keys in docs[key]._id) {
                    getdata += '<tr><td>' + docs[key]._id[i] + '</td><td>' + docs[key].ItemID[i] + '</td><td>' + ItemNames[docs[key].ItemID[i]] + '</td><td>' + docs[key].Stack[i] + '</td></tr>';
                    i++;
                }
            }
            sendData = { ShowViews: req.session.views, MsgServer: 'Show My Item', data_row: getdata, CheckLogin: JSON.parse(true) };
            client.close();
            res.render('myitem', { sendData });
        });

        console.log("Mongo Run Success");
    }); // End MongoDB Connect
});

app.get('/register', (req, res) => {
    if (req.session.acc_Email)
        res.redirect('/');
    else {
        var sendData = {};
        sendData = { ShowViews: ++req.session.views, MsgServer: 'On Register Page' };
        res.render('register', { sendData });
    }
});

app.post('/action_register', function (req, res) {

    var sendData = {};
    if (req.session.views)
        req.session.views++;
    else
        req.session.views = 1;

    mongodb.MongoClient.connect(server, { useNewUrlParser: true }, function (err, client) {
        if (err) { console.log(err); }
        var db = client.db('KaLaMangDB');

        if (String(req.body.username).length > 3 && String(req.body.username).length < 13)
            if (String(req.body.password).length > 3 && String(req.body.password).length < 13)
                if (String(req.body.nickname).length > 3 && String(req.body.nickname).length < 13)
                    if (String(req.body.company).length > 3 && String(req.body.company).length < 13)
                        if (Number.isInteger(parseInt(req.body.age)))
                            if (req.body.password == req.body.passwordverify) {
                                db.collection('KLM_Account').findOne({ UserName: req.body.username }, function (err, docuuser) {
                                    if (err) { console.log(err); }
                                    if (!docuuser) {
                                        db.collection('KLM_Account').findOne({ NickName: req.body.nickname }, function (err, docnick) {
                                            if (err) { console.log(err); }
                                            if (!docnick) {
                                                console.log('OK Register');
                                                db.collection('KLM_Account').find({}).sort({ AID: -1 }).limit(1).toArray(function (err, doclastaid) {
                                                    if (err) { console.log(err); }
                                                    db.collection('KLM_Account').insertOne({ "AID": (parseInt(doclastaid[0].AID) + 1), "UserName": req.body.username, "PassWord": req.body.password, "NickName": req.body.nickname, "Age": parseInt(req.body.age), "Company": req.body.company, "ActiveDate": new Date() });

                                                    sendData = { ShowViews: req.session.views, MsgServer: 'Register Complete' };
                                                    res.render('register', { sendData });
                                                }); // End MongoDB Get Last AID
                                            }
                                            else { // Nickname already
                                                console.log('Nickname already');
                                                sendData = { ShowViews: req.session.views, MsgServer: 'Nickname already' };
                                                res.render('register', { sendData });
                                            }
                                        }); // End MongoDB Check NickName already
                                    }
                                    else { // Username already
                                        console.log('Username already');
                                        sendData = { ShowViews: req.session.views, MsgServer: 'Username already' };
                                        res.render('register', { sendData });
                                    }
                                }); // End MongoDB Check UserName already
                            }
                            else {
                                // Password Verify wrong
                                console.log('Password Verify wrong');
                                sendData = { ShowViews: req.session.views, MsgServer: 'Password verify not match' };
                                res.render('register', { sendData });
                            }
                        else {
                            // Age is not Int
                            console.log('Age is not Int');
                            sendData = { ShowViews: req.session.views, MsgServer: 'Input Age only number' };
                            res.render('register', { sendData });
                        }
                    else {
                        // Company Digit
                        console.log('Company Digit');
                        sendData = { ShowViews: req.session.views, MsgServer: 'Company Digit not correct' };
                        res.render('register', { sendData });
                    }
                else {
                    // Nickname Digit
                    console.log('Nickname Digit');
                    sendData = { ShowViews: req.session.views, MsgServer: 'Nickname Digit not correct' };
                    res.render('register', { sendData });
                }
            else {
                // Password Digit
                console.log('Password Digit');
                sendData = { ShowViews: req.session.views, MsgServer: 'Password Digit not correct' };
                res.render('register', { sendData });
            }
        else {
            // UserName Digit
            console.log('UserName Digit');
            sendData = { ShowViews: req.session.views, MsgServer: 'UserName Digit not correct' };
            res.render('register', { sendData });
        }

        console.log("Mongo Run Success");
    }); // End MongoDB Connect    
}); // End action_pass

app.get('/main_profile', (req, res) => {
    //req.session.views++;
    if (!req.session.acc_Email)
        res.redirect('/');
    else {
        req.session.ejs_Page = 'Profile';

        mongodb.MongoClient.connect(server, { useNewUrlParser: true }, function (err, client) {
            if (err) { console.log(err); }
            var db = client.db('KaLaMangDB');

            db.collection('KLM_Account').findOne({ UserName: req.session.acc_Email }, function (err, doc) {
                if (err) { console.log(err); }

                var sendData = {};
                var ChangeProfileShow = false;

                if (req.query.editprofile)
                    ChangeProfileShow = true;

                sendData = { ShowViews: req.session.views, MsgServer: 'Show Data My Profile', GetNickName: doc.NickName, GetGradeID: doc.Age, GetActionChangeProfile: ChangeProfileShow };

                //console.log(sendData);
                client.close();
                req.session.ejs_sendData = sendData;
                res.redirect('/');
            });
            console.log("Mongo Run Success");
        }); // End MongoDB Connect
        //res.redirect('/');
    }
});

app.get('/main_myitem', (req, res) => {
    //req.session.views++;
    if (!req.session.acc_Email)
        res.redirect('/');
    else {
        req.session.ejs_Page = 'MyItem';
        mongodb.MongoClient.connect(server, { useNewUrlParser: true }, function (err, client) {
            if (err) { console.log(err); }
            var db = client.db('KaLaMangDB');
    
            var sendData = {};
            var getdata = '';
    
            db.collection('KLM_Account').aggregate([
                { $match: { 'UserName': req.session.acc_Email } },
                {
                    $lookup: {
                        from: 'KLM_Inventory',
                        localField: 'AID',
                        foreignField: 'OwnAID',
                        as: 'new_invent'
                    }
                },
                {
                    $lookup: {
                        from: 'KLM_ItemID',
                        localField: 'new_invent.ItemID',
                        foreignField: 'ItemID',
                        as: 'new_itemid'
                    }
                },
                {
                    $group: {
                        _id: '$new_invent.InvenID',
                        ItemID: { $first: '$new_invent.ItemID' },
                        ItemName: { $first: '$new_itemid' },
                        Stack: { $first: '$new_invent.Stack' },
                    }
                }
            ]).toArray(function (err, docs) {
                if (err) { console.log(err); }
    
                // Create Array ItemName
                var ItemNames = {};
    
                for (var key in docs) {
                    var i = 0;
    
                    var ItemInfoArray = docs[key].ItemName;
                    for (var keyItem in ItemInfoArray) {
                        ItemNames[ItemInfoArray[keyItem].ItemID] = ItemInfoArray[keyItem].ItemName;
                    }
    
                    for (var keys in docs[key]._id) {
                        getdata += '<tr><td>' + docs[key]._id[i] + '</td><td>' + docs[key].ItemID[i] + '</td><td>' + ItemNames[docs[key].ItemID[i]] + '</td><td>' + docs[key].Stack[i] + '</td></tr>';
                        i++;
                    }
                }
                sendData = { ShowViews: req.session.views, MsgServer: 'Show My Item', data_row: getdata, CheckLogin: JSON.parse(true) };
                client.close();
                req.session.ejs_sendData = sendData;
                res.redirect('/');
            });
    
            console.log("Mongo Run Success");
        }); // End MongoDB Connect
    }
});

app.get('/home', (req, res) => {
    req.session.views++;
    req.session.ejs_Page = '';
    req.session.ejs_sendData = '';
    res.redirect('/');
});

function m_Test() {
    mongodb.MongoClient.connect(server, { useNewUrlParser: true }, function (err, client) {
        if (err) { console.log(err); }
        var db = client.db('KaLaMangDB');

        db.collection('KLM_Account').aggregate([
            { $match: { AID: 1 } },
            {
                $lookup: {
                    from: 'KLM_Inventory',
                    localField: 'AID',
                    foreignField: 'OwnAID',
                    as: 'new_invent'
                }
            }
        ]).toArray(function (err, docs) {
            if (err) { console.log(err); }
            var i = 0;
            for (var key1 in docs) {
                var data_invent = docs[key1].new_invent;
                /*for (var key2 in data_invent) {
                    let m_ItemName = '';
                    db.collection('KLM_ItemID').findOne({ ItemID: data_invent[key2].ItemID }, function (err, doc) {
                        if (err) { console.log(err); }
                        m_ItemName = doc.ItemName;
                        //console.log('InvenID: ' + data_invent[key2].InvenID + ' | ItemID: ' + data_invent[key2].ItemID + ' | ItemName: ' + doc.ItemName + ' | Stack: ' + data_invent[key2].Stack);
                    });
                    //console.log('InvenID: ' + data_invent[key2].InvenID + ' | ItemID: ' + data_invent[key2].ItemID + ' | Stack: ' + data_invent[key2].Stack);
                    console.log('InvenID: ' + data_invent[key2].InvenID + ' | ItemID: ' + data_invent[key2].ItemID + ' | ItemName: ' + m_ItemName + ' | Stack: ' + data_invent[key2].Stack);
                }
                //console.log('InvenID: ' + docs[key1].InvenID + ' | ItemID: ' + docs[key1].ItemID + ' | Stack: ' + docs[key1].Stack);
                i++;*/
                console.log('1Inve');
            }
            client.close();
        }, db.collection('KLM_ItemID').findOne({ ItemID: { $gt: 1 } }, function (err, doc) {
            if (err) { console.log(err); }
            var m_ItemName = doc.ItemName;
            console.log('InvenID1:');
        }), db.collection('KLM_ItemID').findOne({ ItemID: 1 }, function (err, doc) {
            if (err) { console.log(err); }

            console.log('InvenID2:');
        }));

        console.log("Mongo Run Success");
    }); // End MongoDB Connect
}